'use strict'
function CodeCache () {
  ;(this.codeCache = {}), (this.load = function (a, b) {
    if (this.codeCache[a]) return void b(null, this.codeCache[a])
    try {
      fs.readFile(
        a,
        function (c, d) {
          c || (this.codeCache[a] = d.toString()), b(null, this.codeCache[a])
        }.bind(this)
      )
    } catch (c) {
      b(c)
    }
  })
}
const fs = require('fs')
module.exports = new CodeCache()
