'use strict'
var ipcRenderer = require('electron').ipcRenderer
require('andapp-shellapp-sdk').renderer.preload()
var path = require('path'),
  codeCache = require('../javascripts/CodeCache'),
  GranblueLocations = require('../javascripts/granbluelocations')
  ;(global.AndAppClient = {
    appUpdate: function () {
      ipcRenderer.sendToHost('appUpdate')
    },
    toggleWindow: function (a) {
      var b = document.querySelector('html').style.zoom
      ipcRenderer.sendToHost('onToggleSubmenu', { isOpen: a, zoom: b })
    },
    hideBackground: function () {
      ipcRenderer.sendToHost('hideBackground')
    },
    setLanguage: function (a) {
      ipcRenderer.sendToHost('setLanguage', a)
    }
  }), window.addEventListener('DOMContentLoaded', function () {
    ;(document.ondragover = document.ondrop = document.ondragstart = function (a) {
      return a.preventDefault(), !1
    }), (document.onmousedown = function (a) {
      typeof a.buttons !== 'undefined' && a.buttons === 8 && window.history.back()
    })
    var a = document.querySelector('html').style.zoom,
      b = path.join(__dirname, '..', 'stylesheets/override.css')
    if (
    (
      codeCache.load(b, function (a, b) {
        a || (document.head.innerHTML += '<style>' + b + '</style>')
      }),
      !GranblueLocations.isValid(location.href)
    )
  ) {
      return void ipcRenderer.sendToHost('initWindow', {
        status: { isOpen: !1, isNoSubmenu: !0 },
        zoom: a
      })
    }
    var b = path.join(__dirname, '..', 'javascripts/jquery-2.0.3.min.js')
    codeCache.load(b, function (a, b) {
      a || (document.head.innerHTML += '<script>' + b + '</script>')
    })
    var c = JSON.parse(window.localStorage.getItem('gbf_pcsbm')),
      d = c && c.is_open ? !0 : !1,
      e = document.getElementById('submenu') === null
    ipcRenderer.sendToHost('initWindow', {
      status: { isOpen: d, isNoSubmenu: e },
      zoom: a
    })
  }), ipcRenderer.on('overrideFontFamily', function () {
    var a = document.getElementsByTagName('body')[0],
      b = getComputedStyle(a, '').fontFamily,
      c = '"FOT-ニューロダン Pro M", '
    b.indexOf(c) === 0 && ((b = b.split(c)[1]), (a.style.fontFamily = b))
  }), ipcRenderer.on('message', function (a, b, c) {
    ;(b = b || {}), window.postMessage(b, c)
  })
