function init () {
  function a () {
    clearTimeout(window.lock), (window.lock = setTimeout(function () {
      clearTimeout(window.lock), delete window.lock
    }, NAVIGATE_INTERVAL))
  }
  function b () {
    return !!window.lock
  }
  function c (a) {
    var b = d.getContentBounds()
    return (b.height = parseInt(a.height, 10)), (b.width = parseInt(
      a.width,
      10
    )), { x: b.x, y: b.y, width: b.width, height: b.height }
  }
  var d = remote.getCurrentWindow(),
    e = d.getContentBounds()
  ;(window.bW = e.width), (window.bH = e.height), (webView.style.height =
    e.height - 10 + 'px'), (WINDOW_MARGIN_W =
    d.getSize()[0] - d.getContentSize()[0])
  var f = {
      size: [
        { postMsg: 'sizeS_close', bounds: { width: 400, height: 579 } },
        { postMsg: 'sizeS_open', bounds: { width: 720, height: 579 } },
        { postMsg: 'sizeS_other', bounds: { width: 336, height: 579 } },
        {
          postMsg: 'sizeM_close',
          bounds: {
            width: 592,
            height:
              screen.height > 961 ? 861 : screen.height - BASIC_MARGIN_H + 10
          }
        },
        {
          postMsg: 'sizeM_open',
          bounds: {
            width: 1072,
            height:
              screen.height > 961 ? 861 : screen.height - BASIC_MARGIN_H + 10
          }
        },
        {
          postMsg: 'sizeM_other',
          bounds: {
            width: 496,
            height:
              screen.height > 961 ? 861 : screen.height - BASIC_MARGIN_H + 10
          }
        },
        {
          postMsg: 'sizeL',
          bounds: {
            width: 784,
            height:
              screen.height > 1236 ? 1136 : screen.height - BASIC_MARGIN_H + 10
          }
        },
        {
          postMsg: 'sizeL_open',
          bounds: {
            width: 1424,
            height:
              screen.height > 1236 ? 1136 : screen.height - BASIC_MARGIN_H + 10
          }
        },
        {
          postMsg: 'sizeL_other',
          bounds: {
            width: 656,
            height:
              screen.height > 1236 ? 1136 : screen.height - BASIC_MARGIN_H + 10
          }
        }
      ]
    },
    g = d.getMaximumSize()
  WIN_MAX_WIDTH_CLOSE = g[0]
  var h = WIN_MAX_WIDTH_CLOSE - WINDOW_MARGIN_W - BASIC_MARGIN_W
  ;(WIN_MAX_WIDTH_OPEN =
    Math.round(WIDTH_OPEN_SUBMENU * h / WIDTH_CLOSE_SUBMENU) +
    BASIC_MARGIN_W +
    WINDOW_MARGIN_W), (WIN_MAX_WIDTH_OTHER =
    2 * (h / 2 - WIDTH_SUBMENU_BUTTON) + BASIC_MARGIN_W + WINDOW_MARGIN_W)
  var i = d.getMinimumSize()
  WIN_MIN_WIDTH_CLOSE = i[0]
  var j = WIN_MIN_WIDTH_CLOSE - WINDOW_MARGIN_W - BASIC_MARGIN_W
  ;(WIN_MIN_WIDTH_OPEN =
    Math.round(WIDTH_OPEN_SUBMENU * j / WIDTH_CLOSE_SUBMENU) +
    BASIC_MARGIN_W +
    WINDOW_MARGIN_W), (WIN_MIN_WIDTH_OTHER =
    WIN_MIN_WIDTH_CLOSE - WIDTH_SUBMENU_BUTTON), (WIN_MAX_HEIGHT =
    screen.height), (WIN_MIN_HEIGHT = i[1])
  var k = function (a) {
      a.target.className = a.target.className.replace('off', 'on')
    },
    l = function (a) {
      a.target.className = a.target.className.replace('on', 'off')
    },
    m = ['reload', 'mypage', 'back']
  m.forEach(function (a) {
    $('#footer-' + a).addEventListener(
      'mouseover',
      k
    ), $('#footer-' + a).addEventListener('mouseout', l)
  })
  var n = ['s', 'm', 'l']
  n.forEach(function (a) {
    $('#btn-resize-' + a).addEventListener(
      'mouseover',
      k
    ), $('#btn-resize-' + a).addEventListener('mouseout', l)
  }), $('#footer-back').addEventListener('click', function () {
    b() !== !0 && (webView.goBack(), a())
  }), $('#footer-reload').addEventListener('click', function () {
    b() !== !0 &&
      (
        window.debugMode === !0
          ? webView.reloadIgnoringCache()
          : webView.reload(),
        a()
      )
  }), $('#footer-mypage').addEventListener('click', function () {
    b() !== !0 &&
      (
        webView.loadURL(
          (['src'].value = BASE_URL + '?_=' + new Date().getTime() + '#mypage')
        ),
        a()
      )
  }), $('#btn-resize-s').addEventListener('click', function () {
    var a = window.isNoSubmenu === !0 ? 2 : window.isOpenSubmenu === !0 ? 1 : 0
    d.setContentBounds(c(f.size[a].bounds))
  }), $('#btn-resize-m').addEventListener('click', function () {
    var a = window.isNoSubmenu === !0 ? 5 : window.isOpenSubmenu === !0 ? 4 : 3
    d.setContentBounds(c(f.size[a].bounds))
  }), $('#btn-resize-l').addEventListener('click', function () {
    var a = window.isNoSubmenu === !0 ? 8 : window.isOpenSubmenu === !0 ? 7 : 6
    d.setContentBounds(c(f.size[a].bounds))
  }), (window.shouldReload = !0), (window.isNoSubmenu = !0), initFooter(), gbfConfig.get('lang') ==
    'en' &&
    $('#footer-mypage').classList.add(
      'en'
    ), (document.ondragover = document.ondrop = document.ondragstart = function (
    a
  ) {
      return a.preventDefault(), !1
    }), d.on('resize', onBoundsChanged), d.on('minimize', onMinimize), d.on(
    'restore',
    onRestore
  ), wvInit.call(this, webView, 'sizeS')
}
function onMinimize () {
  window.isMinimized = !0
}
function onRestore () {
  window.isMinimized = !1
}
function executeScript (a, b) {
  var c = path.join(__dirname, b)
  codeCache.load(c, function (b, c) {
    b || a.executeJavaScript(c, !1)
  })
}
function postMessage (a, b, c) {
  a.send('message', b, c)
}
function initFooter () {
  var a = 1
  if (window.footerZoom) a = window.footerZoom
  else {
    var b
    ;(b =
      window.isNoSubmenu === !0
        ? WIDTH_GAME_BASE
        : window.isOpenSubmenu === !0
          ? WIDTH_OPEN_SUBMENU
          : WIDTH_CLOSE_SUBMENU), (a = (window.bW - BASIC_MARGIN_W) / b)
  }
  $('#footer').setAttribute('style', 'zoom:' + a + ';')
}
function setLanguage (a) {
  var b = gbfConfig.get('lang')
  b !== a &&
    (
      a == 'en'
        ? $('#footer-mypage').classList.add('en')
        : $('#footer-mypage').classList.remove('en'),
      gbfConfig.set('lang', a)
    )
}
var electron = require('electron'),
  remote = electron.remote,
  ipcRenderer = electron.ipcRenderer,
  shell = electron.shell,
  path = require('path'),
  andAppSDKRenderer = require('andapp-shellapp-sdk').renderer,
  codeCache = require('./javascripts/CodeCache'),
  gbfConfig = remote.getGlobal('gbfConfig')
  ;(window.timerResize = !1), (window.bW = window.bH = null), (window.debugMode = !1), (window.debugBuild = !1)
const BASE_URL = 'http://gbf.game.mbga.jp/'
var WIN_MAX_WIDTH_CLOSE,
  WIN_MAX_WIDTH_OPEN,
  WIN_MAX_WIDTH_OTHER,
  WIN_MIN_WIDTH_CLOSE,
  WIN_MIN_WIDTH_OPEN,
  WIN_MIN_WIDTH_OTHER,
  WIN_MAX_HEIGHT,
  WIN_MIN_HEIGHT,
  WINDOW_MARGIN_W
const WIDTH_GAME_BASE = 320,
  WIDTH_SUBMENU_BUTTON = 64,
  WIDTH_OPEN_SUBMENU = 704,
  WIDTH_CLOSE_SUBMENU = 384,
  BASIC_MARGIN_H = 100,
  BASIC_MARGIN_W = 16,
  NAVIGATE_INTERVAL = 200
var webView = null
window.onload = function () {
  webView = $('#webview-game')
  var a = (function () {
    return 'Mozilla/5.0 (Linux; Android 4.4.4; 401SO Build/23.0.H.0.302) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/34.0.0.0 Mobile Safari/537.36 GbfAA/1.0'
  })()
  andAppSDKRenderer.init(webView, { overrideUAString: a }, function (a) {
    a &&
      (
        alert(a.meaage),
        remote.app.quit()
      ), (webView.preload = 'javascripts/preload.js'), (webView.src = BASE_URL), init()
  })
}
var $ = function (a) {
    return document.querySelector(a)
  },
  wvInit = function (a, b) {
    function c (a) {
      var b = {
        isOpenSubmenu: window.isOpenSubmenu,
        isNoSubmenu: window.isNoSubmenu
      }
      ;(window.isOpenSubmenu = a.isOpen), (window.isNoSubmenu = a.isNoSubmenu)
      var c = remote.getCurrentWindow()
      typeof b.isOpenSubmenu === 'undefined'
        ? (
            (window.shouldReload = !0),
            window.isNoSubmenu === !0
              ? (
                  c.setMaximumSize(WIN_MAX_WIDTH_OTHER, WIN_MAX_HEIGHT),
                  c.setMinimumSize(WIN_MIN_WIDTH_OTHER, WIN_MIN_HEIGHT),
                  d(WIN_MAX_WIDTH_OTHER, WIN_MIN_WIDTH_OTHER)
                )
              : window.isOpenSubmenu === !0
                ? (
                    c.setMaximumSize(WIN_MAX_WIDTH_OPEN, WIN_MAX_HEIGHT),
                    c.setMinimumSize(WIN_MIN_WIDTH_OPEN, WIN_MIN_HEIGHT),
                    d(WIN_MAX_WIDTH_OPEN, WIN_MIN_WIDTH_OPEN)
                  )
                : d(WIN_MAX_WIDTH_CLOSE, WIN_MIN_WIDTH_CLOSE)
          )
        : window.isOpenSubmenu != b.isOpenSubmenu
          ? window.isOpenSubmenu === !0 ? e() : f()
          : window.isNoSubmenu === !0
            ? (
                c.setMaximumSize(WIN_MAX_WIDTH_OTHER, WIN_MAX_HEIGHT),
                c.setMinimumSize(WIN_MIN_WIDTH_OTHER, WIN_MIN_HEIGHT),
                d(WIN_MAX_WIDTH_OTHER, WIN_MIN_WIDTH_OTHER)
              )
            : window.isOpenSubmenu === !0
              ? (
                  c.setMaximumSize(WIN_MAX_WIDTH_OPEN, WIN_MAX_HEIGHT),
                  c.setMinimumSize(WIN_MIN_WIDTH_OPEN, WIN_MIN_HEIGHT),
                  d(WIN_MAX_WIDTH_OPEN, WIN_MIN_WIDTH_OPEN)
                )
              : (
                  c.setMaximumSize(WIN_MAX_WIDTH_CLOSE, WIN_MAX_HEIGHT),
                  c.setMinimumSize(WIN_MIN_WIDTH_CLOSE, WIN_MIN_HEIGHT),
                  d(WIN_MAX_WIDTH_CLOSE, WIN_MIN_WIDTH_CLOSE)
                ), window.isNoSubmenu === !0
        ? $('#appVer').classList.remove('on-sub')
        : $('#appVer').classList.add('on-sub')
    }
    function d (a, b) {
      var c = remote.getCurrentWindow(),
        d = c.getContentBounds(),
        e = d.width + BASIC_MARGIN_W
      if (b > e) {
        var f = { x: d.x, y: d.y, width: b, height: d.height }
        ;(window.shouldReload = !0), c.setContentBounds(f)
      } else if (e > a) {
        var f = { x: d.x, y: d.y, width: a, height: d.height }
        ;(window.shouldReload = !0), c.setContentBounds(f)
      }
      initFooter()
    }
    function e () {
      window.isOpenSubmenu = !0
      var a = remote.getCurrentWindow(),
        b = a.getContentBounds(),
        c = b.width - BASIC_MARGIN_W,
        d =
          Math.round(c * WIDTH_OPEN_SUBMENU / WIDTH_CLOSE_SUBMENU) +
          BASIC_MARGIN_W
        ;(window.shouldReload = !1), a.setMaximumSize(
        WIN_MAX_WIDTH_OPEN,
        WIN_MAX_HEIGHT
      ), a.setMinimumSize(WIN_MIN_WIDTH_OPEN, WIN_MIN_HEIGHT), d >
        WIN_MAX_WIDTH_OPEN - WINDOW_MARGIN_W &&
        ((window.shouldReload = !0), (d = WIN_MAX_WIDTH_OPEN - WINDOW_MARGIN_W))
      var e = { x: b.x, y: b.y, width: d, height: b.height }
      a.setContentBounds(e)
    }
    function f () {
      window.isOpenSubmenu = !1
      var a = remote.getCurrentWindow(),
        b = a.getContentBounds(),
        c = b.width - BASIC_MARGIN_W,
        d =
          Math.round(c * WIDTH_CLOSE_SUBMENU / WIDTH_OPEN_SUBMENU) +
          BASIC_MARGIN_W
        ;(window.shouldReload = !1), a.setMaximumSize(
        WIN_MAX_WIDTH_CLOSE,
        WIN_MAX_HEIGHT
      ), a.setMinimumSize(
        WIN_MIN_WIDTH_CLOSE,
        WIN_MIN_HEIGHT
      ), WIN_MIN_WIDTH_CLOSE - WINDOW_MARGIN_W > d &&
        (
          (window.shouldReload = !0),
          (d = WIN_MIN_WIDTH_CLOSE - WINDOW_MARGIN_W)
        )
      var e = { x: b.x, y: b.y, width: d, height: b.height }
      a.setContentBounds(e)
    }
    a.addEventListener('dom-ready', function () {
      $('#footer').classList.remove(
        'hide'
      ), remote.getGlobal('isMac') === !0 && a.send('overrideFontFamily')
    }), a.addEventListener('did-finish-load', function () {
      executeScript(
        a,
        'javascripts/create-touch.js'
      ), executeScript(a, 'javascripts/app-override.js'), postMessage(a, { command: b }, '*'), (window.debugMode === !0 || window.debugBuild === !0) && a.openDevTools()
    }), a.addEventListener('new-window', function (b) {
      ;/^https?:\/\/(?:[^.\/]+\.)*mbga\.jp\//.test(b.url)
        ? a.loadURL(b.url)
        : shell.openExternal(b.url)
    }), a.addEventListener('ipc-message', function (a) {
      switch (a.channel) {
        case 'onToggleSubmenu':
          ;(window.footerZoom =
            a.args[0].zoom), a.args[0].isOpen === !0 ? e() : f()
          break
        case 'initWindow':
          ;(window.footerZoom = a.args[0].zoom), c(a.args[0].status)
          break
        case 'appUpdate':
          ipcRenderer.send('appUpdate')
          break
        case 'hideBackground':
          $('#footer').classList.add('hide')
          break
        case 'setLanguage':
          setLanguage(a.args[0])
      }
    }), ($('#appVer').innerHTML = 'Ver. ' + remote.getGlobal('appVersionName'))
  },
  onBoundsChanged = function () {
    timerResize !== !1 && clearTimeout(timerResize)
    var a = window.shouldReload === !0 ? 200 : 0
    timerResize = setTimeout(function () {
      if (window.isMinimized !== !0) {
        var a = remote.getCurrentWindow().getContentBounds(),
          b = a.width,
          c = a.height
        if (window.bW != b || window.bH != c) {
          ;(window.bW = b), (window.bH = c), ipcRenderer.send(
            'onBoundsChanged',
            { width: window.bW, height: window.bH }
          )
          var d = $('#webview-game')
          initFooter(), (d.style.height =
            c - 10 + 'px'), window.shouldReload === !0 &&
            d.reload(), (window.shouldReload = !0)
        }
      }
    }, a)
  }
