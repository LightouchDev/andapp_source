function getRandomToken () {
  var a = require('crypto')
  return a.randomBytes(32).toString('hex')
}
function getErrorMessageForDisplay (a) {
  const b = [-30101, -30201]
  b.some(function (b) {
    return a.code === b
  })
}
function onClose () {
  gbfConfig.set('width', windowBounds.width), gbfConfig.set(
    'height',
    windowBounds.height
  )
}
function onClosed () {
  ;(mainWindow = null), app.quit()
}
function displayErrorAndQuit (a) {
  _displayMessage(
    { type: 'error', message: a.code, detail: a.message },
    function () {
      app.quit()
    }
  )
}
function _displayMessage (a, b) {
  var c = {
    type: a.type || 'info',
    buttons: a.buttons || ['OK'],
    message: a.message,
    detail: a.detail
  }
  dialog.showMessageBox(c, b)
}
var electron = require('electron'),
  app = electron.app,
  BrowserWindow = electron.BrowserWindow,
  shell = electron.shell,
  dialog = electron.dialog,
  ipcMain = electron.ipcMain,
  mainWindow = null
const WhiteList = require('./whitelist.js')
var windowBounds,
  shouldQuit = app.makeSingleInstance(function (a, b) {
    mainWindow &&
      (mainWindow.isMinimized() && mainWindow.restore(), mainWindow.focus())
  })
shouldQuit && app.quit(), (global.isMac =
  process.platform == 'darwin'), global.isMac ||
  app.on('browser-window-created', function (a, b) {
    b.setMenu(null)
  })
var shellAppSDKMain = require('andapp-shellapp-sdk').main,
  gbfConfig = new (require('./PersistenceKVS'))('gbf-config.json')
gbfConfig.load(), (global.gbfConfig = gbfConfig), process.on(
  'unhandledRejection',
  function (a) {}
), process.on('uncaughtException', function (a) {}), app.on('ready', function () {
  ;(global.appVersionName =
    shellAppSDKMain.getAppVersionName() || '1.0.0'), shellAppSDKMain.init(
      {
        mobageAppId: '12016007',
        sessionPartition: 'persist:granbluefantasyusers',
        onBeforeSendHeaders: function (a) {
          ;(a.requestHeaders['X-ChromeApp-TID'] = gbfConfig.get(
          'chromeId'
        )), (a.requestHeaders['X-AndApp'] = '1'), (a.requestHeaders[
          'X-AndApp-Version'
        ] =
          global.appVersionName)
        },
        locationValidatorParams: {
          additionalWhiteList: WhiteList,
          onNavigatedToInvalidLocation: function (a, b, c) {
            shell.openExternal(c)
          }
        }
      },
    function (a) {
      if (a) {
        const b = getErrorMessageForDisplay(a)
        return void (b
          ? displayErrorAndQuit(b)
          : process.nextTick(function () {
            app.quit()
          }))
      }
      gbfConfig.loaded.then(function () {
        tracking.call(), getConfig.call()
      }), app.on('window-all-closed', function () {
        process.platform != 'darwin' && app.quit()
      })
    }
  )
})
var tracking = function () {
  var a = gbfConfig.get('chromeId')
  a
    ? !gbfConfig.get('suchTracking')
    : ((a = getRandomToken()), gbfConfig.set('chromeId', a))
}
ipcMain.on('appUpdate', function () {
  shellAppSDKMain.openAppDetailPage(function () {
    app.quit()
  })
}), ipcMain.on('onBoundsChanged', function (a, b) {
  windowBounds = b
})
var getConfig = function () {
    mainWindow = createWin.call(this, {
      width: gbfConfig.get('width'),
      height: gbfConfig.get('height')
    })
  },
  createWin = function (a) {
    var b = a.width || 400,
      c = a.height || 570
    windowBounds = { width: b, height: c }
    require('path')
    return (win = new BrowserWindow({
      minWidth: 400,
      minHeight: 570,
      maxWidth: 784,
      width: b,
      height: c,
      useContentSize: !0,
      resizable: !0
    })), win.loadURL('file://' + __dirname + '/../index.html'), win.on(
      'close',
      onClose
    ), win.on('closed', onClosed), win
  }
