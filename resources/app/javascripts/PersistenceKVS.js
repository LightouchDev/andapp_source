'use strict'
function PersistenceKVS (a) {
  ;(this._filePath = path.join(
    app.getPath('userData'),
    a
  )), (this._config = null), (this._loadedPromise = new Promise(
    function (a, b) {
      ;(this._loadResolve = a), (this._loadReject = b)
    }.bind(this)
  )), (this.loaded = this._loadedPromise), (this.get = function (a) {
    return this._config[a]
  }), (this.set = function (a, b) {
    void 0 === b ? delete this._config[a] : (this._config[a] = b)
    try {
      fs.writeFile(this._filePath, JSON.stringify(this._config), function (a) {})
    } catch (c) {}
  }), (this.load = function () {
    this._exists(
      function (a, b) {
        if (b) {
          fs.readFile(
            this._filePath,
            function (a, b) {
              if (a) this._loadReject(a)
              else {
                try {
                  ;(this._config = JSON.parse(String(b))), this._loadResolve()
                } catch (c) {
                  var d = this
                  fs.unlink(this._filePath, function () {
                    d.load()
                  })
                }
              }
            }.bind(this)
          )
        } else {
          try {
            fs.writeFile(
              this._filePath,
              '{}',
              function (a) {
                return a
                  ? void this._loadReject(a)
                  : ((this._config = {}), void this._loadResolve())
              }.bind(this)
            )
          } catch (a) {
            this._loadReject(a)
          }
        }
      }.bind(this)
    )
  }), (this._exists = function (a) {
    try {
      fs.stat(this._filePath, function (b, c) {
        a(b, Boolean(c))
      })
    } catch (b) {
      a(b, !1)
    }
  })
}
const app = require('electron').app,
  fs = require('fs'),
  path = require('path')
module.exports = PersistenceKVS
